<?php namespace DPS\Imgix\Tests;

use DPS\Imgix\UrlBuilder;

class UrlBuilderTest extends TestCase
{
        function testAutoFeatures0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->autoFeatures('enhance');
            $expected = 'https://foo.imgix.net/test.png?auto=enhance';
            $this->assertEquals($expected, $actual);
        }
        
        function testAutoFeaturesReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['auto'] = 1;
            $builder->autoFeatures();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlendAlign0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendAlign('top');
            $expected = 'https://foo.imgix.net/test.png?ba=top';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendAlignReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['ba'] = 1;
            $builder->blendAlign();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlendAlpha0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendAlpha(1);
            $expected = 'https://foo.imgix.net/test.png?balph=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendAlphaReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['balph'] = 1;
            $builder->blendAlpha();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlendCrop0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendCrop('top');
            $expected = 'https://foo.imgix.net/test.png?bc=top';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendCropReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['bc'] = 1;
            $builder->blendCrop();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlendFit0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendFit('unknown');
            $expected = 'https://foo.imgix.net/test.png?bf=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendFitReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['bf'] = 1;
            $builder->blendFit();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBackgroundColor0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->backgroundColor('fff');
            $expected = 'https://foo.imgix.net/test.png?bg=fff';
            $this->assertEquals($expected, $actual);
        }
        
        function testBackgroundColorReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['bg'] = 1;
            $builder->backgroundColor();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlendHeight0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendHeight(1);
            $expected = 'https://foo.imgix.net/test.png?bh=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendHeight1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendHeight(1);
            $expected = 'https://foo.imgix.net/test.png?bh=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendHeightReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['bh'] = 1;
            $builder->blendHeight();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlend0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blend('fff');
            $expected = 'https://foo.imgix.net/test.png?blend=fff';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlend1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blend('unknown');
            $expected = 'https://foo.imgix.net/test.png?blend=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlend2()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blend('unknown');
            $expected = 'https://foo.imgix.net/test.png?blend=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['blend'] = 1;
            $builder->blend();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlur0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blur(1);
            $expected = 'https://foo.imgix.net/test.png?blur=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlurReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['blur'] = 1;
            $builder->blur();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlendMode0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendMode('unknown');
            $expected = 'https://foo.imgix.net/test.png?bm=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendModeReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['bm'] = 1;
            $builder->blendMode();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBorderRadiusInner0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->borderRadiusInner(1);
            $expected = 'https://foo.imgix.net/test.png?border-radius-inner=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBorderRadiusInner1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->borderRadiusInner(1,1,1,1);
            $expected = 'https://foo.imgix.net/test.png?border-radius-inner=1%2C1%2C1%2C1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBorderRadiusInnerReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['border-radius-inner'] = 1;
            $builder->borderRadiusInner();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBorderRadius0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->borderRadius(1);
            $expected = 'https://foo.imgix.net/test.png?border-radius=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBorderRadius1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->borderRadius(1,1,1,1);
            $expected = 'https://foo.imgix.net/test.png?border-radius=1%2C1%2C1%2C1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBorderRadiusReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['border-radius'] = 1;
            $builder->borderRadius();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBorder0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->border(1,'fff');
            $expected = 'https://foo.imgix.net/test.png?border=1%2Cfff';
            $this->assertEquals($expected, $actual);
        }
        
        function testBorderReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['border'] = 1;
            $builder->border();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlendPadding0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendPadding(1);
            $expected = 'https://foo.imgix.net/test.png?bp=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendPaddingReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['bp'] = 1;
            $builder->blendPadding();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBrightness0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->brightness(1);
            $expected = 'https://foo.imgix.net/test.png?bri=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBrightnessReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['bri'] = 1;
            $builder->brightness();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlendSize0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendSize('unknown');
            $expected = 'https://foo.imgix.net/test.png?bs=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendSizeReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['bs'] = 1;
            $builder->blendSize();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlendWidth0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendWidth(1);
            $expected = 'https://foo.imgix.net/test.png?bw=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendWidth1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendWidth(1);
            $expected = 'https://foo.imgix.net/test.png?bw=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendWidthReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['bw'] = 1;
            $builder->blendWidth();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlendX0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendX(1);
            $expected = 'https://foo.imgix.net/test.png?bx=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendXReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['bx'] = 1;
            $builder->blendX();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testBlendY0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->blendY(1);
            $expected = 'https://foo.imgix.net/test.png?by=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testBlendYReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['by'] = 1;
            $builder->blendY();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testClientHints0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->clientHints('width');
            $expected = 'https://foo.imgix.net/test.png?ch=width';
            $this->assertEquals($expected, $actual);
        }
        
        function testClientHintsReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['ch'] = 1;
            $builder->clientHints();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testChromaSubsampling0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->chromaSubsampling(1);
            $expected = 'https://foo.imgix.net/test.png?chromasub=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testChromaSubsamplingReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['chromasub'] = 1;
            $builder->chromaSubsampling();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testColorQuantization0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->colorQuantization(1);
            $expected = 'https://foo.imgix.net/test.png?colorquant=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testColorQuantizationReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['colorquant'] = 1;
            $builder->colorQuantization();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testColorCount0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->colorCount(1);
            $expected = 'https://foo.imgix.net/test.png?colors=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testColorCountReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['colors'] = 1;
            $builder->colorCount();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testContrast0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->contrast(1);
            $expected = 'https://foo.imgix.net/test.png?con=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testContrastReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['con'] = 1;
            $builder->contrast();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testCornerRadius0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->cornerRadius(1);
            $expected = 'https://foo.imgix.net/test.png?corner-radius=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testCornerRadius1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->cornerRadius(1,1,1,1);
            $expected = 'https://foo.imgix.net/test.png?corner-radius=1%2C1%2C1%2C1';
            $this->assertEquals($expected, $actual);
        }
        
        function testCornerRadiusReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['corner-radius'] = 1;
            $builder->cornerRadius();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testCropMode0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->cropMode('top');
            $expected = 'https://foo.imgix.net/test.png?crop=top';
            $this->assertEquals($expected, $actual);
        }
        
        function testCropModeReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['crop'] = 1;
            $builder->cropMode();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testColorspace0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->colorspace('unknown');
            $expected = 'https://foo.imgix.net/test.png?cs=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testColorspaceReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['cs'] = 1;
            $builder->colorspace();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testDownload0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->download('unknown');
            $expected = 'https://foo.imgix.net/test.png?dl=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testDownloadReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['dl'] = 1;
            $builder->download();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testDotsPerInch0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->dotsPerInch(1);
            $expected = 'https://foo.imgix.net/test.png?dpi=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testDotsPerInchReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['dpi'] = 1;
            $builder->dotsPerInch();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testDevicePixelRatio0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->devicePixelRatio(1);
            $expected = 'https://foo.imgix.net/test.png?dpr=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testDevicePixelRatioReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['dpr'] = 1;
            $builder->devicePixelRatio();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testExposure0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->exposure(1);
            $expected = 'https://foo.imgix.net/test.png?exp=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testExposureReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['exp'] = 1;
            $builder->exposure();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testURLExpirationTimestamp0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->uRLExpirationTimestamp('unknown');
            $expected = 'https://foo.imgix.net/test.png?expires=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testURLExpirationTimestampReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['expires'] = 1;
            $builder->uRLExpirationTimestamp();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testFaceIndex0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->faceIndex(1);
            $expected = 'https://foo.imgix.net/test.png?faceindex=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testFaceIndexReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['faceindex'] = 1;
            $builder->faceIndex();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testFacePadding0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->facePadding(1);
            $expected = 'https://foo.imgix.net/test.png?facepad=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testFacePaddingReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['facepad'] = 1;
            $builder->facePadding();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testFaces0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->faces(1);
            $expected = 'https://foo.imgix.net/test.png?faces=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testFacesReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['faces'] = 1;
            $builder->faces();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testFitMode0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->fitMode('unknown');
            $expected = 'https://foo.imgix.net/test.png?fit=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testFitModeReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['fit'] = 1;
            $builder->fitMode();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testFlipDirection0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->flipDirection('unknown');
            $expected = 'https://foo.imgix.net/test.png?flip=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testFlipDirectionReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['flip'] = 1;
            $builder->flipDirection();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testOutputFormat0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->outputFormat('unknown');
            $expected = 'https://foo.imgix.net/test.png?fm=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testOutputFormatReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['fm'] = 1;
            $builder->outputFormat();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testFocalPointCrosshairs0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->focalPointCrosshairs(true);
            $expected = 'https://foo.imgix.net/test.png?fp-debug=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testFocalPointCrosshairsReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['fp-debug'] = 1;
            $builder->focalPointCrosshairs();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testFocalPointXValue0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->focalPointXValue(1);
            $expected = 'https://foo.imgix.net/test.png?fp-x=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testFocalPointXValueReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['fp-x'] = 1;
            $builder->focalPointXValue();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testFocalPointYValue0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->focalPointYValue(1);
            $expected = 'https://foo.imgix.net/test.png?fp-y=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testFocalPointYValueReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['fp-y'] = 1;
            $builder->focalPointYValue();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testFocalPointZoomValue0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->focalPointZoomValue(1);
            $expected = 'https://foo.imgix.net/test.png?fp-z=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testFocalPointZoomValueReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['fp-z'] = 1;
            $builder->focalPointZoomValue();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testGamma0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->gamma(1);
            $expected = 'https://foo.imgix.net/test.png?gam=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testGammaReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['gam'] = 1;
            $builder->gamma();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testHeight0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->height(1);
            $expected = 'https://foo.imgix.net/test.png?h=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testHeight1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->height(1);
            $expected = 'https://foo.imgix.net/test.png?h=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testHeightReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['h'] = 1;
            $builder->height();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testHigh0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->high(1);
            $expected = 'https://foo.imgix.net/test.png?high=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testHighReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['high'] = 1;
            $builder->high();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testHalftone0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->halftone(1);
            $expected = 'https://foo.imgix.net/test.png?htn=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testHalftoneReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['htn'] = 1;
            $builder->halftone();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testHueShift0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->hueShift(1);
            $expected = 'https://foo.imgix.net/test.png?hue=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testHueShiftReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['hue'] = 1;
            $builder->hueShift();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testInvert0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->invert(true);
            $expected = 'https://foo.imgix.net/test.png?invert=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testInvertReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['invert'] = 1;
            $builder->invert();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testLossless0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->lossless(true);
            $expected = 'https://foo.imgix.net/test.png?lossless=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testLosslessReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['lossless'] = 1;
            $builder->lossless();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWatermarkImage0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkImage('unknown');
            $expected = 'https://foo.imgix.net/test.png?mark=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkImage1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkImage('unknown');
            $expected = 'https://foo.imgix.net/test.png?mark=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkImageReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['mark'] = 1;
            $builder->watermarkImage();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWatermarkAlignmentMode0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkAlignmentMode('top');
            $expected = 'https://foo.imgix.net/test.png?markalign=top';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkAlignmentModeReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['markalign'] = 1;
            $builder->watermarkAlignmentMode();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWatermarkAlpha0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkAlpha(1);
            $expected = 'https://foo.imgix.net/test.png?markalpha=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkAlphaReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['markalpha'] = 1;
            $builder->watermarkAlpha();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWatermarkUrlBase0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkUrlBase('unknown');
            $expected = 'https://foo.imgix.net/test.png?markbase=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkUrlBase1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkUrlBase('unknown');
            $expected = 'https://foo.imgix.net/test.png?markbase=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkUrlBaseReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['markbase'] = 1;
            $builder->watermarkUrlBase();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWatermarkFitMode0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkFitMode('unknown');
            $expected = 'https://foo.imgix.net/test.png?markfit=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkFitModeReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['markfit'] = 1;
            $builder->watermarkFitMode();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWatermarkHeight0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkHeight(1);
            $expected = 'https://foo.imgix.net/test.png?markh=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkHeight1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkHeight(1);
            $expected = 'https://foo.imgix.net/test.png?markh=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkHeightReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['markh'] = 1;
            $builder->watermarkHeight();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWatermarkPadding0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkPadding(1);
            $expected = 'https://foo.imgix.net/test.png?markpad=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkPaddingReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['markpad'] = 1;
            $builder->watermarkPadding();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWatermarkScale0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkScale(1);
            $expected = 'https://foo.imgix.net/test.png?markscale=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkScaleReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['markscale'] = 1;
            $builder->watermarkScale();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWatermarkWidth0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkWidth(1);
            $expected = 'https://foo.imgix.net/test.png?markw=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkWidth1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkWidth(1);
            $expected = 'https://foo.imgix.net/test.png?markw=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkWidthReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['markw'] = 1;
            $builder->watermarkWidth();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWatermarkXPosition0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkXPosition(1);
            $expected = 'https://foo.imgix.net/test.png?markx=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkXPositionReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['markx'] = 1;
            $builder->watermarkXPosition();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWatermarkYPosition0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->watermarkYPosition(1);
            $expected = 'https://foo.imgix.net/test.png?marky=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testWatermarkYPositionReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['marky'] = 1;
            $builder->watermarkYPosition();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testMask0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->mask('unknown');
            $expected = 'https://foo.imgix.net/test.png?mask=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testMask1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->mask('unknown');
            $expected = 'https://foo.imgix.net/test.png?mask=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testMask2()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->mask('unknown');
            $expected = 'https://foo.imgix.net/test.png?mask=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testMaskReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['mask'] = 1;
            $builder->mask();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testMaskBackgroundColor0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->maskBackgroundColor('fff');
            $expected = 'https://foo.imgix.net/test.png?maskbg=fff';
            $this->assertEquals($expected, $actual);
        }
        
        function testMaskBackgroundColorReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['maskbg'] = 1;
            $builder->maskBackgroundColor();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testMaximumHeight0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->maximumHeight(1);
            $expected = 'https://foo.imgix.net/test.png?max-h=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testMaximumHeightReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['max-h'] = 1;
            $builder->maximumHeight();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testMaximumWidth0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->maximumWidth(1);
            $expected = 'https://foo.imgix.net/test.png?max-w=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testMaximumWidthReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['max-w'] = 1;
            $builder->maximumWidth();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testMinimumHeight0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->minimumHeight(1);
            $expected = 'https://foo.imgix.net/test.png?min-h=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testMinimumHeightReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['min-h'] = 1;
            $builder->minimumHeight();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testMinimumWidth0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->minimumWidth(1);
            $expected = 'https://foo.imgix.net/test.png?min-w=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testMinimumWidthReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['min-w'] = 1;
            $builder->minimumWidth();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testMonochrome0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->monochrome('fff');
            $expected = 'https://foo.imgix.net/test.png?mono=fff';
            $this->assertEquals($expected, $actual);
        }
        
        function testMonochromeReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['mono'] = 1;
            $builder->monochrome();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testNoiseBlur0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->noiseBlur(1);
            $expected = 'https://foo.imgix.net/test.png?nr=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testNoiseBlurReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['nr'] = 1;
            $builder->noiseBlur();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testNoiseSharpen0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->noiseSharpen(1);
            $expected = 'https://foo.imgix.net/test.png?nrs=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testNoiseSharpenReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['nrs'] = 1;
            $builder->noiseSharpen();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testOrientation0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->orientation(1);
            $expected = 'https://foo.imgix.net/test.png?or=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testOrientationReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['or'] = 1;
            $builder->orientation();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testPadding0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->padding(1);
            $expected = 'https://foo.imgix.net/test.png?pad=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testPaddingReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['pad'] = 1;
            $builder->padding();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testPdfPageNumber0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->pdfPageNumber(1);
            $expected = 'https://foo.imgix.net/test.png?page=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testPdfPageNumberReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['page'] = 1;
            $builder->pdfPageNumber();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testPaletteExtraction0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->paletteExtraction('unknown');
            $expected = 'https://foo.imgix.net/test.png?palette=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testPaletteExtractionReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['palette'] = 1;
            $builder->paletteExtraction();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testCssPrefix0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->cssPrefix('unknown');
            $expected = 'https://foo.imgix.net/test.png?prefix=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testCssPrefixReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['prefix'] = 1;
            $builder->cssPrefix();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testPixellate0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->pixellate(1);
            $expected = 'https://foo.imgix.net/test.png?px=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testPixellateReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['px'] = 1;
            $builder->pixellate();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testOutputQuality0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->outputQuality(1);
            $expected = 'https://foo.imgix.net/test.png?q=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testOutputQualityReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['q'] = 1;
            $builder->outputQuality();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testCropRectangle0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->cropRectangle('unknown');
            $expected = 'https://foo.imgix.net/test.png?rect=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testCropRectangleReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['rect'] = 1;
            $builder->cropRectangle();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testRotationAngle0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->rotationAngle(1);
            $expected = 'https://foo.imgix.net/test.png?rot=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testRotationAngleReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['rot'] = 1;
            $builder->rotationAngle();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testSaturation0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->saturation(1);
            $expected = 'https://foo.imgix.net/test.png?sat=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testSaturationReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['sat'] = 1;
            $builder->saturation();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testSepia0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->sepia(1);
            $expected = 'https://foo.imgix.net/test.png?sepia=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testSepiaReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['sepia'] = 1;
            $builder->sepia();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testShadow0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->shadow(1);
            $expected = 'https://foo.imgix.net/test.png?shad=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testShadowReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['shad'] = 1;
            $builder->shadow();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testSharpen0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->sharpen(1);
            $expected = 'https://foo.imgix.net/test.png?sharp=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testSharpenReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['sharp'] = 1;
            $builder->sharpen();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTrimMode0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->trimMode('unknown');
            $expected = 'https://foo.imgix.net/test.png?trim=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testTrimModeReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['trim'] = 1;
            $builder->trimMode();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTrimColor0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->trimColor('fff');
            $expected = 'https://foo.imgix.net/test.png?trimcolor=fff';
            $this->assertEquals($expected, $actual);
        }
        
        function testTrimColorReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['trimcolor'] = 1;
            $builder->trimColor();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTrimMeanDifference0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->trimMeanDifference(1);
            $expected = 'https://foo.imgix.net/test.png?trimmd=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testTrimMeanDifferenceReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['trimmd'] = 1;
            $builder->trimMeanDifference();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTrimStandardDeviation0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->trimStandardDeviation(1);
            $expected = 'https://foo.imgix.net/test.png?trimsd=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testTrimStandardDeviationReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['trimsd'] = 1;
            $builder->trimStandardDeviation();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTrimTolerance0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->trimTolerance(1);
            $expected = 'https://foo.imgix.net/test.png?trimtol=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testTrimToleranceReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['trimtol'] = 1;
            $builder->trimTolerance();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextString0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textString('unknown');
            $expected = 'https://foo.imgix.net/test.png?txt=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextStringReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txt'] = 1;
            $builder->textString();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextAlign0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textAlign('top');
            $expected = 'https://foo.imgix.net/test.png?txtalign=top';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextAlignReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtalign'] = 1;
            $builder->textAlign();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextClipping0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textClipping('start');
            $expected = 'https://foo.imgix.net/test.png?txtclip=start';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextClippingReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtclip'] = 1;
            $builder->textClipping();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextColor0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textColor('fff');
            $expected = 'https://foo.imgix.net/test.png?txtclr=fff';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextColorReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtclr'] = 1;
            $builder->textColor();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextFitMode0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textFitMode('unknown');
            $expected = 'https://foo.imgix.net/test.png?txtfit=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextFitModeReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtfit'] = 1;
            $builder->textFitMode();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextFont0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textFont('unknown');
            $expected = 'https://foo.imgix.net/test.png?txtfont=unknown';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextFontReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtfont'] = 1;
            $builder->textFont();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextLeading0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textLeading(1);
            $expected = 'https://foo.imgix.net/test.png?txtlead=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextLeadingReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtlead'] = 1;
            $builder->textLeading();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextLigatures0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textLigatures(1);
            $expected = 'https://foo.imgix.net/test.png?txtlig=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextLigaturesReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtlig'] = 1;
            $builder->textLigatures();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextOutline0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textOutline(1);
            $expected = 'https://foo.imgix.net/test.png?txtline=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextOutlineReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtline'] = 1;
            $builder->textOutline();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextOutlineColor0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textOutlineColor('fff');
            $expected = 'https://foo.imgix.net/test.png?txtlineclr=fff';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextOutlineColorReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtlineclr'] = 1;
            $builder->textOutlineColor();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextPadding0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textPadding(1);
            $expected = 'https://foo.imgix.net/test.png?txtpad=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextPaddingReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtpad'] = 1;
            $builder->textPadding();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextShadow0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textShadow(1);
            $expected = 'https://foo.imgix.net/test.png?txtshad=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextShadowReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtshad'] = 1;
            $builder->textShadow();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextFontSize0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textFontSize(1);
            $expected = 'https://foo.imgix.net/test.png?txtsize=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextFontSizeReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtsize'] = 1;
            $builder->textFontSize();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextTracking0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textTracking(1);
            $expected = 'https://foo.imgix.net/test.png?txttrack=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextTrackingReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txttrack'] = 1;
            $builder->textTracking();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testTextWidth0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->textWidth(1);
            $expected = 'https://foo.imgix.net/test.png?txtwidth=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testTextWidthReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['txtwidth'] = 1;
            $builder->textWidth();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testUnsharpMask0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->unsharpMask(1);
            $expected = 'https://foo.imgix.net/test.png?usm=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testUnsharpMaskReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['usm'] = 1;
            $builder->unsharpMask();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testUnsharpMaskRadius0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->unsharpMaskRadius(1);
            $expected = 'https://foo.imgix.net/test.png?usmrad=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testUnsharpMaskRadiusReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['usmrad'] = 1;
            $builder->unsharpMaskRadius();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testVibrance0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->vibrance(1);
            $expected = 'https://foo.imgix.net/test.png?vib=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testVibranceReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['vib'] = 1;
            $builder->vibrance();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
        function testWidth0()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->width(1);
            $expected = 'https://foo.imgix.net/test.png?w=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testWidth1()
        {
            $actual = (string) UrlBuilder::file('test.png', ['foo.imgix.net'])->width(1);
            $expected = 'https://foo.imgix.net/test.png?w=1';
            $this->assertEquals($expected, $actual);
        }
        
        function testWidthReset()
        {
            $builder = UrlBuilder::file('test.png', ['foo.imgix.net']);
            $builder->attribute['w'] = 1;
            $builder->width();
    
            $actual = (string) $builder;
            $expected = 'https://foo.imgix.net/test.png';
            $this->assertEquals($expected, $actual);
        }
    
}