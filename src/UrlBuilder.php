<?php
/**
 * This file is generated with `make generate`
 * DO NOT EDIT | generated at Wed, 28 Mar 2018 15:59:53 +1030
 */
namespace DPS\Imgix;

use Imgix\UrlBuilder as Builder;
use Imgix\ShardStrategy;

/**
 * UrlBuilder wrapper around the imgix api
 * 
 * @version 8.1.0
 * @link    https://docs.imgix.com/apis/url
 */
class UrlBuilder
{
    protected $filename = "";
    public $attributes = [];
    protected $builder;

    public function __construct($domains)
    {
        $this->builder = new Builder($domains, true, '', ShardStrategy::CRC, false);
    }

    public function file($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    public function url()
    {
        return $this->builder->createUrl($this->filename, $this->attributes);
    }

    public function __toString()
    {
        return $this->url();
    }

    protected function expectInteger($value) {
        return is_numeric($value);
    }

    protected function expectNumber($value) {
        return is_numeric($value);
    }

    protected function expectHexColor($value) {
        $patten = "/^([A-Fa-f0-9]{8}|[A-Fa-f0-9]{6}|[A-Fa-f0-9]{4}|[A-Fa-f0-9]{3})$/";
        return preg_match($patten, $value);
    }

    protected function expectString($value) {
        return !!$value;
    }

    protected function expectList($value, $allowed) {
        return in_array($value, $allowed);
    }
    
    protected function expectBoolean($value) {
        return is_bool($value);
    }

    protected function expectUrl($value) {
        return filter_var($value, FILTER_VALIDATE_URL);
    }

    protected function expectPath($value) {
        return $this->expectString($value);
    }

    protected function expectUnitScalar($value) {
        return $this->expectNumber($value);
    }

    protected function expectTimestamp($value) {
        return true;
    }

    /**
     * Applies automatic enhancements to images.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/auto
     * 
     * @return $this
     */
    public function autoFeatures($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['auto']);
            return $this;
        }
    
        if (!($this->expectList($v0, ['enhance' , 'format' , 'redeye' , 'compress']))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['auto'] = $v0;
        return $this;
    }

    /**
     * Colors the background of padded images.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/bg
     * 
     * @return $this
     */
    public function backgroundColor($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['bg']);
            return $this;
        }
    
        if (!($this->expectHexColor($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['bg'] = $v0;
        return $this;
    }

    /**
     * Specifies the location of the blend image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/blend
     * 
     * @return $this
     */
    public function blend($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['blend']);
            return $this;
        }
    
        if (!($this->expectHexColor($v0) || $this->expectUrl($v0) || $this->expectPath($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['blend'] = $v0;
        return $this;
    }

    /**
     * Changes the blend alignment relative to the parent image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/ba
     * 
     * @return $this
     */
    public function blendAlign($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['ba']);
            return $this;
        }
    
        if (!($this->expectList($v0, ['top' , 'bottom' , 'middle' , 'left' , 'right' , 'center']))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['ba'] = $v0;
        return $this;
    }

    /**
     * Changes the alpha of the blend image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/balph
     * 
     * @return $this
     */
    public function blendAlpha($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['balph']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['balph'] = $v0;
        return $this;
    }

    /**
     * Specifies the type of crop for blend images.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/bc
     * 
     * @return $this
     */
    public function blendCrop($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['bc']);
            return $this;
        }
    
        if (!($this->expectList($v0, ['top' , 'bottom' , 'left' , 'right' , 'faces']))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['bc'] = $v0;
        return $this;
    }

    /**
     * Specifies the fit mode for blend images.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/bf
     * 
     * @return $this
     */
    public function blendFit($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['bf']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['bf'] = $v0;
        return $this;
    }

    /**
     * Adjusts the height of the blend image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/bh
     * 
     * @return $this
     */
    public function blendHeight($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['bh']);
            return $this;
        }
    
        if (!($this->expectInteger($v0) || $this->expectUnitScalar($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['bh'] = $v0;
        return $this;
    }

    /**
     * Sets the blend mode for a blend image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/bm
     * 
     * @return $this
     */
    public function blendMode($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['bm']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['bm'] = $v0;
        return $this;
    }

    /**
     * Applies padding to the blend image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/bp
     * 
     * @return $this
     */
    public function blendPadding($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['bp']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['bp'] = $v0;
        return $this;
    }

    /**
     * Adjusts the size of the blend image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/bs
     * 
     * @return $this
     */
    public function blendSize($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['bs']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['bs'] = $v0;
        return $this;
    }

    /**
     * Adjusts the width of the blend image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/bw
     * 
     * @return $this
     */
    public function blendWidth($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['bw']);
            return $this;
        }
    
        if (!($this->expectInteger($v0) || $this->expectUnitScalar($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['bw'] = $v0;
        return $this;
    }

    /**
     * Adjusts the x-offset of the blend image relative to its parent.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/bx
     * 
     * @return $this
     */
    public function blendX($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['bx']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['bx'] = $v0;
        return $this;
    }

    /**
     * Adjusts the y-offset of the blend image relative to its parent.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/blending/by
     * 
     * @return $this
     */
    public function blendY($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['by']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['by'] = $v0;
        return $this;
    }

    /**
     * Applies a gaussian blur to an image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/stylize/blur
     * 
     * @return $this
     */
    public function blur($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['blur']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['blur'] = $v0;
        return $this;
    }

    /**
     * Applies a border to an image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/border-and-padding/border
     * 
     * @return $this
     */
    public function border($v0 = null, $v1 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['border']);
            return $this;
        }
    
        if (!($this->expectInteger($v0) && $this->expectHexColor($v1))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['border'] = implode(',', array_filter([$v0, $v1]));
        return $this;
    }

    /**
     * Sets the outer radius of the image's border in pixels.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/border-and-padding/border-radius
     * 
     * @return $this
     */
    public function borderRadius($v0 = null, $v1 = null, $v2 = null, $v3 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['border-radius']);
            return $this;
        }
    
        if (!($this->expectInteger($v0) || $this->expectInteger($v0) && $this->expectInteger($v1) && $this->expectInteger($v2) && $this->expectInteger($v3))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['border-radius'] = implode(',', array_filter([$v0, $v1, $v2, $v3]));
        return $this;
    }

    /**
     * Sets the inner radius of the image's border in pixels.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/border-and-padding/border-radius-inner
     * 
     * @return $this
     */
    public function borderRadiusInner($v0 = null, $v1 = null, $v2 = null, $v3 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['border-radius-inner']);
            return $this;
        }
    
        if (!($this->expectInteger($v0) || $this->expectInteger($v0) && $this->expectInteger($v1) && $this->expectInteger($v2) && $this->expectInteger($v3))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['border-radius-inner'] = implode(',', array_filter([$v0, $v1, $v2, $v3]));
        return $this;
    }

    /**
     * Adjusts the brightness of the source image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/bri
     * 
     * @return $this
     */
    public function brightness($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['bri']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['bri'] = $v0;
        return $this;
    }

    /**
     * Specifies the output chroma subsampling rate.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/format/chromasub
     * 
     * @return $this
     */
    public function chromaSubsampling($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['chromasub']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['chromasub'] = $v0;
        return $this;
    }

    /**
     * Sets one or more Client-Hints headers
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/format/ch
     * 
     * @return $this
     */
    public function clientHints($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['ch']);
            return $this;
        }
    
        if (!($this->expectList($v0, ['width' , 'dpr' , 'save-data']))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['ch'] = $v0;
        return $this;
    }

    /**
     * Specifies how many colors to include in a palette-extraction response.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/color-palette/colors
     * 
     * @return $this
     */
    public function colorCount($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['colors']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['colors'] = $v0;
        return $this;
    }

    /**
     * Limits the number of unique colors in an image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/format/colorquant
     * 
     * @return $this
     */
    public function colorQuantization($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['colorquant']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['colorquant'] = $v0;
        return $this;
    }

    /**
     * Specifies the color space of the output image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/format/cs
     * 
     * @return $this
     */
    public function colorspace($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['cs']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['cs'] = $v0;
        return $this;
    }

    /**
     * Adjusts the contrast of the source image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/con
     * 
     * @return $this
     */
    public function contrast($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['con']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['con'] = $v0;
        return $this;
    }

    /**
     * Specifies the radius value for a rounded corner mask.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/mask/corner-radius
     * 
     * @return $this
     */
    public function cornerRadius($v0 = null, $v1 = null, $v2 = null, $v3 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['corner-radius']);
            return $this;
        }
    
        if (!($this->expectInteger($v0) || $this->expectInteger($v0) && $this->expectInteger($v1) && $this->expectInteger($v2) && $this->expectInteger($v3))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['corner-radius'] = implode(',', array_filter([$v0, $v1, $v2, $v3]));
        return $this;
    }

    /**
     * Specifies how to crop an image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/size/crop
     * 
     * @return $this
     */
    public function cropMode($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['crop']);
            return $this;
        }
    
        if (!($this->expectList($v0, ['top' , 'bottom' , 'left' , 'right' , 'faces' , 'entropy' , 'edges' , 'focalpoint']))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['crop'] = $v0;
        return $this;
    }

    /**
     * Crops an image to a specified rectangle.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/size/rect
     * 
     * @return $this
     */
    public function cropRectangle($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['rect']);
            return $this;
        }
    
        if (!($this->expectInteger($v0) && $this->expectInteger($v1) && $this->expectInteger($v2) && $this->expectInteger($v3))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['rect'] = $v0;
        return $this;
    }

    /**
     * Specifies a CSS prefix for all classes in palette-extraction.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/color-palette/prefix
     * 
     * @return $this
     */
    public function cssPrefix($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['prefix']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['prefix'] = $v0;
        return $this;
    }

    /**
     * Adjusts the device-pixel ratio of the output image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/dpr
     * 
     * @return $this
     */
    public function devicePixelRatio($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['dpr']);
            return $this;
        }
    
        if (!($this->expectNumber($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['dpr'] = $v0;
        return $this;
    }

    /**
     * Sets the DPI value in the EXIF header.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/format/dpi
     * 
     * @return $this
     */
    public function dotsPerInch($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['dpi']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['dpi'] = $v0;
        return $this;
    }

    /**
     * Forces a URL to use send-file in its response.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/format/dl
     * 
     * @return $this
     */
    public function download($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['dl']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['dl'] = $v0;
        return $this;
    }

    /**
     * Adjusts the exposure of the output image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/exp
     * 
     * @return $this
     */
    public function exposure($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['exp']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['exp'] = $v0;
        return $this;
    }

    /**
     * Selects a face to crop to.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/face-detection/faceindex
     * 
     * @return $this
     */
    public function faceIndex($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['faceindex']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['faceindex'] = $v0;
        return $this;
    }

    /**
     * Adjusts padding around a selected face.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/face-detection/facepad
     * 
     * @return $this
     */
    public function facePadding($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['facepad']);
            return $this;
        }
    
        if (!($this->expectNumber($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['facepad'] = $v0;
        return $this;
    }

    /**
     * Specifies that face data should be included in output when combined with `fm=json`.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/face-detection/faces
     * 
     * @return $this
     */
    public function faces($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['faces']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['faces'] = $v0;
        return $this;
    }

    /**
     * Specifies how to map the source image to the output image dimensions.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/size/fit
     * 
     * @return $this
     */
    public function fitMode($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['fit']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['fit'] = $v0;
        return $this;
    }

    /**
     * Flips an image on a specified axis.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/rotation/flip
     * 
     * @return $this
     */
    public function flipDirection($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['flip']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['flip'] = $v0;
        return $this;
    }

    /**
     * Displays crosshairs identifying the location of the set focal point
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/focalpoint-crop/fp-debug
     * 
     * @return $this
     */
    public function focalPointCrosshairs($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['fp-debug']);
            return $this;
        }
    
        if (!($this->expectBoolean($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['fp-debug'] = $v0;
        return $this;
    }

    /**
     * Sets the relative horizontal value for the focal point of an image
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/focalpoint-crop/fp-x
     * 
     * @return $this
     */
    public function focalPointXValue($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['fp-x']);
            return $this;
        }
    
        if (!($this->expectUnitScalar($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['fp-x'] = $v0;
        return $this;
    }

    /**
     * Sets the relative vertical value for the focal point of an image
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/focalpoint-crop/fp-y
     * 
     * @return $this
     */
    public function focalPointYValue($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['fp-y']);
            return $this;
        }
    
        if (!($this->expectUnitScalar($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['fp-y'] = $v0;
        return $this;
    }

    /**
     * Sets the relative zoom value for the focal point of an image
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/focalpoint-crop/fp-z
     * 
     * @return $this
     */
    public function focalPointZoomValue($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['fp-z']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['fp-z'] = $v0;
        return $this;
    }

    /**
     * Adjusts the gamma of the source image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/gam
     * 
     * @return $this
     */
    public function gamma($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['gam']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['gam'] = $v0;
        return $this;
    }

    /**
     * Applies a half-tone effect to the source image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/stylize/htn
     * 
     * @return $this
     */
    public function halftone($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['htn']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['htn'] = $v0;
        return $this;
    }

    /**
     * Adjusts the height of the output image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/size/h
     * 
     * @return $this
     */
    public function height($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['h']);
            return $this;
        }
    
        if (!($this->expectInteger($v0) || $this->expectUnitScalar($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['h'] = $v0;
        return $this;
    }

    /**
     * Adjusts the highlights of the source image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/high
     * 
     * @return $this
     */
    public function high($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['high']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['high'] = $v0;
        return $this;
    }

    /**
     * Adjusts the hue of the source image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/hue
     * 
     * @return $this
     */
    public function hueShift($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['hue']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['hue'] = $v0;
        return $this;
    }

    /**
     * Inverts the colors on the source image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/invert
     * 
     * @return $this
     */
    public function invert($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['invert']);
            return $this;
        }
    
        if (!($this->expectBoolean($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['invert'] = $v0;
        return $this;
    }

    /**
     * Specifies that the output image should be a lossless variant.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/format/lossless
     * 
     * @return $this
     */
    public function lossless($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['lossless']);
            return $this;
        }
    
        if (!($this->expectBoolean($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['lossless'] = $v0;
        return $this;
    }

    /**
     * Defines the type of mask and specifies the URL if that type is selected.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/mask
     * 
     * @return $this
     */
    public function mask($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['mask']);
            return $this;
        }
    
        if (!($this->expectString($v0) || $this->expectUrl($v0) || $this->expectPath($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['mask'] = $v0;
        return $this;
    }

    /**
     * Colors the background of the transparent mask area of images
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/mask/mask-bg
     * 
     * @return $this
     */
    public function maskBackgroundColor($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['maskbg']);
            return $this;
        }
    
        if (!($this->expectHexColor($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['maskbg'] = $v0;
        return $this;
    }

    /**
     * Specifies the maximum height of the output image in pixels.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/size/max-h
     * 
     * @return $this
     */
    public function maximumHeight($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['max-h']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['max-h'] = $v0;
        return $this;
    }

    /**
     * Specifies the maximum width of the output image in pixels.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/size/max-w
     * 
     * @return $this
     */
    public function maximumWidth($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['max-w']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['max-w'] = $v0;
        return $this;
    }

    /**
     * Specifies the minimum height of the output image in pixels.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/size/min-h
     * 
     * @return $this
     */
    public function minimumHeight($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['min-h']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['min-h'] = $v0;
        return $this;
    }

    /**
     * Specifies the minimum width of the output image in pixels.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/size/min-w
     * 
     * @return $this
     */
    public function minimumWidth($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['min-w']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['min-w'] = $v0;
        return $this;
    }

    /**
     * Applies a monochrome effect to the source image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/stylize/mono
     * 
     * @return $this
     */
    public function monochrome($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['mono']);
            return $this;
        }
    
        if (!($this->expectHexColor($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['mono'] = $v0;
        return $this;
    }

    /**
     * Reduces the noise in an image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/noise-reduction/nr
     * 
     * @return $this
     */
    public function noiseBlur($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['nr']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['nr'] = $v0;
        return $this;
    }

    /**
     * Provides a threshold by which to sharpen an image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/noise-reduction/nrs
     * 
     * @return $this
     */
    public function noiseSharpen($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['nrs']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['nrs'] = $v0;
        return $this;
    }

    /**
     * Changes the image orientation.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/rotation/or
     * 
     * @return $this
     */
    public function orientation($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['or']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['or'] = $v0;
        return $this;
    }

    /**
     * Changes the format of the output image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/format/fm
     * 
     * @return $this
     */
    public function outputFormat($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['fm']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['fm'] = $v0;
        return $this;
    }

    /**
     * Adjusts the quality of an output image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/format/q
     * 
     * @return $this
     */
    public function outputQuality($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['q']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['q'] = $v0;
        return $this;
    }

    /**
     * Pads an image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/border-and-padding/pad
     * 
     * @return $this
     */
    public function padding($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['pad']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['pad'] = $v0;
        return $this;
    }

    /**
     * Specifies an output format for palette-extraction.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/color-palette/palette
     * 
     * @return $this
     */
    public function paletteExtraction($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['palette']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['palette'] = $v0;
        return $this;
    }

    /**
     * Selects a page from a PDF for display.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/pdf-page-number
     * 
     * @return $this
     */
    public function pdfPageNumber($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['page']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['page'] = $v0;
        return $this;
    }

    /**
     * Applies a pixelation effect to an image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/stylize/px
     * 
     * @return $this
     */
    public function pixellate($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['px']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['px'] = $v0;
        return $this;
    }

    /**
     * Rotates an image by a specified number of degrees.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/rotation/rot
     * 
     * @return $this
     */
    public function rotationAngle($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['rot']);
            return $this;
        }
    
        if (!($this->expectNumber($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['rot'] = $v0;
        return $this;
    }

    /**
     * Adjusts the saturation of an image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/sat
     * 
     * @return $this
     */
    public function saturation($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['sat']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['sat'] = $v0;
        return $this;
    }

    /**
     * Applies a sepia effect to an image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/stylize/sepia
     * 
     * @return $this
     */
    public function sepia($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['sepia']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['sepia'] = $v0;
        return $this;
    }

    /**
     * Adjusts the highlights of the source image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/shad
     * 
     * @return $this
     */
    public function shadow($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['shad']);
            return $this;
        }
    
        if (!($this->expectNumber($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['shad'] = $v0;
        return $this;
    }

    /**
     * Adjusts the sharpness of the source image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/sharp
     * 
     * @return $this
     */
    public function sharpen($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['sharp']);
            return $this;
        }
    
        if (!($this->expectNumber($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['sharp'] = $v0;
        return $this;
    }

    /**
     * Sets the vertical and horizontal alignment of rendered text relative to the base image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtalign
     * 
     * @return $this
     */
    public function textAlign($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtalign']);
            return $this;
        }
    
        if (!($this->expectList($v0, ['top' , 'middle' , 'bottom' , 'left' , 'center' , 'right']))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtalign'] = $v0;
        return $this;
    }

    /**
     * Sets the clipping properties of rendered text.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtclip
     * 
     * @return $this
     */
    public function textClipping($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtclip']);
            return $this;
        }
    
        if (!($this->expectList($v0, ['start' , 'middle' , 'end' , 'ellipsis']))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtclip'] = $v0;
        return $this;
    }

    /**
     * Specifies the color of rendered text.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtclr
     * 
     * @return $this
     */
    public function textColor($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtclr']);
            return $this;
        }
    
        if (!($this->expectHexColor($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtclr'] = $v0;
        return $this;
    }

    /**
     * Specifies the fit approach for rendered text.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtfit
     * 
     * @return $this
     */
    public function textFitMode($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtfit']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtfit'] = $v0;
        return $this;
    }

    /**
     * Selects a font for rendered text.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtfont
     * 
     * @return $this
     */
    public function textFont($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtfont']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtfont'] = $v0;
        return $this;
    }

    /**
     * Sets the font size of rendered text.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtsize
     * 
     * @return $this
     */
    public function textFontSize($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtsize']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtsize'] = $v0;
        return $this;
    }

    /**
     * Sets the leading (line spacing) for rendered text. Only works on the multi-line text endpoint.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtlead
     * 
     * @return $this
     */
    public function textLeading($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtlead']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtlead'] = $v0;
        return $this;
    }

    /**
     * Controls the level of ligature substitution
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtlig
     * 
     * @return $this
     */
    public function textLigatures($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtlig']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtlig'] = $v0;
        return $this;
    }

    /**
     * Outlines the rendered text with a specified color.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtline
     * 
     * @return $this
     */
    public function textOutline($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtline']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtline'] = $v0;
        return $this;
    }

    /**
     * Specifies a text outline color.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtlineclr
     * 
     * @return $this
     */
    public function textOutlineColor($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtlineclr']);
            return $this;
        }
    
        if (!($this->expectHexColor($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtlineclr'] = $v0;
        return $this;
    }

    /**
     * Specifies the padding (in device-independent pixels) between a textbox and the edges of the base image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtpad
     * 
     * @return $this
     */
    public function textPadding($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtpad']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtpad'] = $v0;
        return $this;
    }

    /**
     * Applies a shadow to rendered text.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtshad
     * 
     * @return $this
     */
    public function textShadow($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtshad']);
            return $this;
        }
    
        if (!($this->expectNumber($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtshad'] = $v0;
        return $this;
    }

    /**
     * Sets the text string to render.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txt
     * 
     * @return $this
     */
    public function textString($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txt']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txt'] = $v0;
        return $this;
    }

    /**
     * Sets the tracking (letter spacing) for rendered text. Only works on the multi-line text endpoint.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtlead
     * 
     * @return $this
     */
    public function textTracking($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txttrack']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txttrack'] = $v0;
        return $this;
    }

    /**
     * Sets the width of rendered text.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/text/txtwidth
     * 
     * @return $this
     */
    public function textWidth($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['txtwidth']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['txtwidth'] = $v0;
        return $this;
    }

    /**
     * Specifies a trim color on a trim operation.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/trim/trimcolor
     * 
     * @return $this
     */
    public function trimColor($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['trimcolor']);
            return $this;
        }
    
        if (!($this->expectHexColor($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['trimcolor'] = $v0;
        return $this;
    }

    /**
     * Specifies the mean difference on a trim operation.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/trim/trimmd
     * 
     * @return $this
     */
    public function trimMeanDifference($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['trimmd']);
            return $this;
        }
    
        if (!($this->expectNumber($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['trimmd'] = $v0;
        return $this;
    }

    /**
     * Trims the source image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/trim/trim
     * 
     * @return $this
     */
    public function trimMode($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['trim']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['trim'] = $v0;
        return $this;
    }

    /**
     * Specifies the standard deviation on a trim operation.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/trim/trimsd
     * 
     * @return $this
     */
    public function trimStandardDeviation($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['trimsd']);
            return $this;
        }
    
        if (!($this->expectNumber($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['trimsd'] = $v0;
        return $this;
    }

    /**
     * Specifies the tolerance on a trim operation.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/trim/trimtol
     * 
     * @return $this
     */
    public function trimTolerance($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['trimtol']);
            return $this;
        }
    
        if (!($this->expectNumber($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['trimtol'] = $v0;
        return $this;
    }

    /**
     * A Unix timestamp specifying a UTC time. Requests made to this URL after that time will output a 404 status code.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/expires
     * 
     * @return $this
     */
    public function uRLExpirationTimestamp($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['expires']);
            return $this;
        }
    
        if (!($this->expectTimestamp($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['expires'] = $v0;
        return $this;
    }

    /**
     * Sharpens the source image using an unsharp mask.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/usm
     * 
     * @return $this
     */
    public function unsharpMask($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['usm']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['usm'] = $v0;
        return $this;
    }

    /**
     * Specifies the radius for an unsharp mask operation.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/usmrad
     * 
     * @return $this
     */
    public function unsharpMaskRadius($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['usmrad']);
            return $this;
        }
    
        if (!($this->expectNumber($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['usmrad'] = $v0;
        return $this;
    }

    /**
     * Adjusts the vibrance of an image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/adjustment/vib
     * 
     * @return $this
     */
    public function vibrance($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['vib']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['vib'] = $v0;
        return $this;
    }

    /**
     * Changes the watermark alignment relative to the parent image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/watermark/markalign
     * 
     * @return $this
     */
    public function watermarkAlignmentMode($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['markalign']);
            return $this;
        }
    
        if (!($this->expectList($v0, ['top' , 'middle' , 'bottom' , 'left' , 'center' , 'right']))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['markalign'] = $v0;
        return $this;
    }

    /**
     * Changes the alpha of the watermark image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/watermark/markalpha
     * 
     * @return $this
     */
    public function watermarkAlpha($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['markalpha']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['markalpha'] = $v0;
        return $this;
    }

    /**
     * Specifies the fit mode for watermark images.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/watermark/markfit
     * 
     * @return $this
     */
    public function watermarkFitMode($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['markfit']);
            return $this;
        }
    
        if (!($this->expectString($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['markfit'] = $v0;
        return $this;
    }

    /**
     * Adjusts the height of the watermark image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/watermark/markh
     * 
     * @return $this
     */
    public function watermarkHeight($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['markh']);
            return $this;
        }
    
        if (!($this->expectInteger($v0) || $this->expectUnitScalar($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['markh'] = $v0;
        return $this;
    }

    /**
     * Specifies the location of the watermark image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/watermark/mark
     * 
     * @return $this
     */
    public function watermarkImage($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['mark']);
            return $this;
        }
    
        if (!($this->expectUrl($v0) || $this->expectPath($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['mark'] = $v0;
        return $this;
    }

    /**
     * Applies padding to the watermark image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/watermark/markpad
     * 
     * @return $this
     */
    public function watermarkPadding($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['markpad']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['markpad'] = $v0;
        return $this;
    }

    /**
     * Adjusts the scale of the watermark image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/watermark/markscale
     * 
     * @return $this
     */
    public function watermarkScale($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['markscale']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['markscale'] = $v0;
        return $this;
    }

    /**
     * Changes base URL of the watermark image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/watermark/markbase
     * 
     * @return $this
     */
    public function watermarkUrlBase($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['markbase']);
            return $this;
        }
    
        if (!($this->expectUrl($v0) || $this->expectPath($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['markbase'] = $v0;
        return $this;
    }

    /**
     * Adjusts the width of the watermark image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/watermark/markw
     * 
     * @return $this
     */
    public function watermarkWidth($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['markw']);
            return $this;
        }
    
        if (!($this->expectInteger($v0) || $this->expectUnitScalar($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['markw'] = $v0;
        return $this;
    }

    /**
     * Adjusts the x-offset of the watermark image relative to its parent.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/watermark/markx
     * 
     * @return $this
     */
    public function watermarkXPosition($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['markx']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['markx'] = $v0;
        return $this;
    }

    /**
     * Adjusts the y-offset of the watermark image relative to its parent.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/watermark/marky
     * 
     * @return $this
     */
    public function watermarkYPosition($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['marky']);
            return $this;
        }
    
        if (!($this->expectInteger($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['marky'] = $v0;
        return $this;
    }

    /**
     * Adjusts the width of the output image.
     * You can unset the value by passing nothing or null
     * 
     * @see https://docs.imgix.com/apis/url/size/w
     * 
     * @return $this
     */
    public function width($v0 = null): UrlBuilder
    {
        if ($v0 === null) {
            unset($this->attributes['w']);
            return $this;
        }
    
        if (!($this->expectInteger($v0) || $this->expectUnitScalar($v0))) {
            // throw new \InvalidArgumentException();
        }
    
        $this->attributes['w'] = $v0;
        return $this;
    }
}
